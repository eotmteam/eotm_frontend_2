
var fs = require('fs');
var gulp = require('gulp');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var rename = require('gulp-rename');
var uglify = require('gulp-uglify');
var htmlmin = require('gulp-htmlmin');
var concat = require('gulp-concat');
var replace = require('gulp-replace');
var bytediff = require('gulp-bytediff');
var runSequence = require('run-sequence');
var ngAnnotate = require('gulp-ng-annotate');
var plumber = require('gulp-plumber');
var livereload = require('gulp-livereload');
var templateCache = require('gulp-angular-templatecache');

var handlebars = require('gulp-compile-handlebars');
var rev = require('gulp-rev');
var revDelete = require('gulp-rev-delete-original');
var revDel = require('rev-del');



// https://github.com/terinjokes/gulp-uglify
var ulgifyConfig = {
  sourceMap: false,
  preserveComments: false,
  mangle: false,
  beautify: false,
  reserveDOMProperties: true,
  mangleProperties: false,
  screwIE8: true
};

// https://github.com/dlmanning/gulp-sass
var sassOptions = {
  errLogToConsole: true,
  outputStyle: 'compressed'
};

// http://caniuse.com/
var autoprefixerOptions = {
  browsers: ['last 2 versions', '> 2%', 'Firefox ESR']
};

// create a handlebars helper to look up
// fingerprinted asset by non-fingerprinted name
var handlebarOpts = {
	helpers: {
		assetPath: function (path, context) {
			return ['/assets/src', context.data.root[path]].join('/');
		}
	}
};

var appHTMLCompileOptions = {
  standalone:true,
  module: 'eotmApp.templates'
}
gulp.task('app-html-compile', function () {
 return gulp.src('../app/**/*.html')
 .pipe(htmlmin({collapseWhitespace: true}))
 .pipe(templateCache('templates.js', appHTMLCompileOptions))
 .pipe(gulp.dest('../app/templates'));
});

gulp.task('delete-old-versions' , () => {
  fs.readdir('src', (err, files) => {
    var manifest = JSON.parse(fs.readFileSync('rev-manifest.json', 'utf8'));
    files.forEach(file => {
      var filePath = 'src/'+file;
      fs.unlink(filePath , (err) => {
        if (err) throw err;
        console.log('path/file.txt was deleted');
      });
    });
  })
})

gulp.task('versioning_html', ['delete-old-versions', 'app-html-compile', 'sass', 'ng-app-concat', 'concat_mainjs'] , function () {
	var manifest = JSON.parse(fs.readFileSync('rev-manifest.json', 'utf8'));

	return gulp.src('index.hbs')
		.pipe(handlebars(manifest, handlebarOpts))
		.pipe(rename('index.html'))
		.pipe(gulp.dest('../'));
});

gulp.task('sass', function() {
  return gulp.src('./scss/*.scss')
  .pipe(sass(sassOptions).on('error', sass.logError))
  .pipe(autoprefixer(autoprefixerOptions))
  .pipe(gulp.dest('src'))
  .pipe(rev())
  .pipe(revDelete())
  .pipe(gulp.dest('src'))
  .pipe(rev.manifest({
    base: 'src../',
    merge: true
  }))
  .pipe(gulp.dest('src'))
  .pipe(livereload());
});

gulp.task('concat_scripts', function() {
  return gulp.src([
      './js/common/hammer.min.js',
      './js/common/angular.min.js',
      './js/common/velocity.js',
      './js/common/angular-ui-router.js',
      './js/common/angular-animate.min.js',
      './js/main.js'
      // './js/common/vue.min.js',
      // './js/common/ng-vue.js'
    ])
    .pipe(concat('common.min.js'))
    // .pipe(uglify())
    .pipe(gulp.dest('js/common'));
});

gulp.task('concat_mainjs', function() {
  return gulp.src([
      './js/main.js'
    ])
    .pipe(concat('main.min.js'))
    .pipe(uglify())
    .pipe(gulp.dest('src'))
    .pipe(rev())
    .pipe(revDelete())
    .pipe(gulp.dest('src'))
    .pipe(rev.manifest({
      base: 'src../',
      merge: true
    }))
    .pipe(gulp.dest('src'));
});

gulp.task('ng-app-concat', function() {
    return gulp.src(['../app/*.js' , '../app/**/*.js'])
	    .pipe(plumber())
			.pipe(concat('appEOTM.js', {newLine: ';'}))
			.pipe(ngAnnotate({add: true}))
	    .pipe(plumber.stop())
      .pipe(gulp.dest('src'))
      .pipe(rev())
      .pipe(revDelete())
      .pipe(gulp.dest('src'))
      .pipe(rev.manifest({
        base: 'src../',
        merge: true
      }))
      .pipe(gulp.dest('src/'));
});

gulp.task('app-prod', ['ng-app-concat'], function() {
	return gulp.src('src/appEOTM-*.js')
		.pipe(plumber())
			.pipe(bytediff.start())
				.pipe(uglify({mangle: true}))
			.pipe(bytediff.stop())
		.pipe(plumber.stop())
		.pipe(gulp.dest('src/'));
});

gulp.task('watch', () => {
    gulp.watch('./scss/*.scss', ['versioning_html'])
    .on('change', (event) => {
      console.log('File ' + event.path + ' was ' + event.type + ', running tasks...');
    });
    gulp.watch(['../app/*.js', '../app/**/*.js'], ['versioning_html'])
    .on('change', (event) => {
      console.log('File ' + event.path + ' was ' + event.type + ', running tasksAPP...');
    });
    gulp.watch(['./js/main.js' ], ['versioning_html'])
    .on('change', (event) => {
      console.log('File ' + event.path + ' was ' + event.type + ', running tasks...');
    });
    runSequence('concat_scripts', 'versioning_html' ,  function(){
      console.log('done');
    });
});

gulp.task('prod', function() {
  runSequence( 'versioning_html' , 'app-prod', function(){
    console.log('production comliled');
  });
});
