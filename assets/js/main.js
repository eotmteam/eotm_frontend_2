(function(eotm, undefined) {

    eotm.init = function() {

      eotm.RWDreset650();

    };


    eotm.thumbsContainerAnimate = function( doubleCheck , from){
      var $article = document.getElementById('article');
      if(window.innerWidth < 650){
        if($article) $article.classList.remove('loading');
        return;
      }
      var $thumbsContainer = document.getElementsByClassName('thumbs-container')[0];
      var $articleView = document.getElementById('articleView');
      var thumbsTop;
      if($article != null)
      $article.classList.remove('loading');
      if( $articleView.clientHeight == 0 ){
        window.innerWidth > 650 ?  thumbsTop = 155 :  thumbsTop = 75 ;
      }else{
        thumbsTop = $articleView.clientHeight + 125 ;
      }
      if(doubleCheck == true  ){
        // console.log('container top', thumbsTop + 'px' , $thumbsContainer.style.top);
      }
      Velocity($thumbsContainer, {top: thumbsTop + 'px'} , {duration: 450});
    };

    eotm.RWDreset650 = function () {
      var winOriginWidth = window.innerWidth;
      window.onresize = function () {
        if (window.innerWidth < 650 && winOriginWidth > 650)
          location.reload();
        if (window.innerWidth > 650 && winOriginWidth < 650)
          location.reload();
      };
    };


}(window.eotm = window.eotm || {}));
