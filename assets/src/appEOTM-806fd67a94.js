var eotmApp = angular.module('eotmApp', [
  'ui.router',
  'ngAnimate',
  'eotmApp.articles',
  'eotmApp.thumbs',
  'eotmApp.menu',
  'eotmApp.templates'
  // 'eotmApp.menuSearch',
  // 'eotmApp.articles.breath'
]);

eotmApp.config(["$stateProvider", function($stateProvider){
  $stateProvider
  .state('about', {
    url: '/about',
    views: {
      'article': {
        templateUrl: 'about/about.tmpl.html',
        controller: 'articlesCtrl as vm'
      }
    }
  })
  .state('products', {
    url: "/products",
    views: {
      'article': {
        templateUrl: "products/products.tmpl.html",
        controller: 'articlesCtrl as vm'
      }
    }
  })
  .state('article', {
    url: "/article/:article",
    views: {
      'article': {
        templateUrl: "articles/article.tmpl.html",
        controller: "articlesCtrl as vm"
      }
    }
  })
  .state('unique', {
    url: "/unique/:unique",
    views: {
      'article': {
        templateUrl: function(stateParams){return 'app/articles/unique/' + stateParams.unique  + '.html'; },
        controller: "articlesCtrl as vm"
      }
    }
  })
  .state('home', {
    url: "/"
  })
  .state('knowledge', {
    url: "/knowledge",
  })
  .state('knowledge.category', {
    url: "/:category"
  })
  .state('works', {
    url: "/works"
  })
  .state('works.category', {
    url: "/:category"
  })
}]);



//**********CONTROLLERS*******************************************
// eotmApp.service('menuData', ['$http' , menuData]);
eotmApp.controller('mainCtrl', ['$scope','$stateParams','$state', '$filter','menuData','$rootScope', '$timeout', '$window',  mainCtrl]);


//*************************************
//MAINCTRL -
//*************************************


function mainCtrl($scope , $stateParams , $state, $filter , menuData , $rootScope, $timeout, $window){
  var vm = this;

  // vm.eotmMainTmpl = "eotmMain.tmpl.html";
  vm.thumbsTmpl = "thumbs/thumbs.tmpl.html";
  vm.menuTmpl = "menu/menu.tmpl.html";

  vm.firstLoadOn = false;
  vm.eotmColor = 'light';
  vm.eotmMoveIn = false;

  vm.mainLoader = function() {
    // vm.firstLoadOn = true;
    vm.loaderHideClass = true;
    $timeout(function () {
      vm.loaderHide = true;
    },350);
    $timeout(function () {
      vm.firstLoadOn = true;
    },600);
  };

  //loader start at 2 sec
  var winLoader = {flag: false, counter: 0 , min: 3 , max: 8 };
  var loaderInt = setInterval(function(){
    winLoader.counter += 1 ;
    if( winLoader.counter > winLoader.min && winLoader.flag == true || winLoader.counter > winLoader.max){
      vm.mainLoader();
      clearInterval(loaderInt);
    }
  },500);

  $timeout(function(){
    winLoader.flag = true;
    $window.eotm.init();
  },250);


  vm.menuMobileState = '';
  // vm.filterReset = filterReset;

  $rootScope.$on('$viewContentLoaded', function(event) {

    $timeout(function() {
      window.eotm.thumbsContainerAnimate(false , 'viewContentLoaded');
    },   $state.current.views ?  1000 :  0 );

  });


  $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams){

    var menuMain = '';
    var menuSub = '';
    var menuMobileState = '';

    if (toParams.category == undefined) {
      if(toState.name == 'home' || toState.name == "products" || toState.name == "about" || toState.name == "article" || toState.name == 'unique'){
        menuMobileState = '';
      }else{
        menuMobileState = toState.name;
      }
      menuMain = toState.name == 'knowledge' ? toState.name : toState.name == 'works' ? toState.name : '';
    }else{
      menuMain =  toState.name.split('.')[0];
      menuSub = toParams.category;
      menuMobileState = toParams.category;
    }

    $rootScope.$broadcast('menuChange', { menuMain: menuMain, menuSub: menuSub, menuMobileState: menuMobileState } );

    if(toState.name == 'article' || toState.name == 'products' || toState.name == 'about'){
      window.pageYOffset > window.innerHeight*1.5 ?  scrollToTop(0) :  scrollToTop(750);
    }else{
      scrollToTop(0);
    }
  });

};


var scrollToTop = function(durationX) {
  // var $menu = document.getElementById('menu');
  var $top = document.getElementsByClassName('main-wrapper')[0];
  Velocity( $top, "scroll", {duration: durationX});
  // if($menu) $menu.classList.remove('hidden');
};

//*************************************
//DIRECTIVES
//*************************************

eotmApp.directive('imgLoader', ["$timeout", function($timeout){
  return{
    link: function(scope, element, attrs){
      element[0].classList.add('loading');
      var loader = document.createElement("span");
      loader.classList.add('loader');
      element[0].parentNode.appendChild(loader);

      element.bind("load",function(e){
        element[0].parentNode.removeChild(loader);
        element[0].classList.remove('loading');
      });
    }
  }
}])
;var articles = angular.module('eotmApp.articles', [
  'ui.router',
  'ngAnimate',
  'eotmApp.articles.breath'
]);

//**********CONTROLLERS*******************************************

eotmApp.service('productsData',['$http' , productsData]);
articles.controller('articlesCtrl', ['$scope','thumbsData','$stateParams','$filter' , 'productsData', '$timeout', articlesCtrl]);

function productsData($http) {
  var model = this;
  var URLS = {
    FETCH: 'data/products.json'
  };
  var products;
  model.getProductsData = function() {
    return $http.get(URLS.FETCH);
  };
};

//*************************************
//ARTICLE CTRL
//*************************************

function articlesCtrl($scope, thumbsData, $stateParams, $filter, productsData, $timeout){
  var vm = this;

  productsData.getProductsData().then(function(result){
    vm.productsData =  result.data;
  });

  thumbsData.getThumbsData().then(function(result){
    var filteredData = result.data.filter(function( obj ) {
      return obj.href == $stateParams.article;
    });
    vm.articleData = filteredData[0];
  });

  vm.articleElementData = articleElementData;
  vm.galleryMobile650 = galleryMobile650;
  vm.hrefImg;

// GALLERY SLIDER IN ARTICLE -> START
  var galleryStartIndex;
  var galleryPicsStore;
  var galleryHrefStore;
  vm.gallery = function(pic, galleryPics, galleryHref, $index) { //directive
    galleryHrefStore = galleryHref;
    galleryPicsStore = galleryPics;

    if(window.innerWidth < 650){
      var imgIndex = $index;
      vm.galleryMobile650(pic, galleryPics, galleryHref, imgIndex);
      return;
    }

    var $body = document.getElementsByTagName('body')[0];
    var firstLoadImg = false;
    var $img = document.getElementById('gallerySliderImg');
    var $gallerySlider = document.getElementById('gallerySlider');
    var $imgLoader = document.getElementsByClassName('gs-img-loader')[0];

    galleryStartIndex = galleryPics.indexOf(pic);
    var hrefImg = "assets/img/gallery/"+ galleryHref +"/" + pic[0];
    vm.hrefImg = "assets/img/gallery/"+ galleryHref +"/" + pic[0];
    $img.src = hrefImg;

    $gallerySlider.classList.add('show');
    $imgLoader.classList.add('show');
    $body.classList.add('no-scroll');

    // put separate -> out of controller
      $img.addEventListener('load', function(){
        setTimeout(function(){
          if(firstLoadImg == true){
            $img.classList.add('move-in');
          }
          $img.classList.remove('move-out');
          $imgLoader.classList.remove('show');
        },350);
        firstLoadImg = true;
      });

    document.addEventListener( 'keyup', function( event ){
      if (event.which === 27){
        vm.galleryClose();
      }
      if (event.which === 39){
        vm.galleryNext(galleryPicsStore, galleryHrefStore);
      };
      if (event.which === 37) {
        vm.galleryPrev(galleryPicsStore, galleryHrefStore);
      };
    });
  };


  vm.galleryNext = function(galleryPics , galleryHref){//directive
    galleryStartIndex = galleryStartIndex + 1;
    if(galleryPics[galleryStartIndex] == undefined) {
      galleryStartIndex = 0;
    }
    var $img = document.getElementById('gallerySliderImg');
    var $imgLoader = document.getElementsByClassName('gs-img-loader')[0];
    var hrefImg = "assets/img/gallery/"+ galleryHref +"/" + galleryPics[galleryStartIndex][0];
    setTimeout(function(){
      $img.src = hrefImg;
    },350);
    $imgLoader.classList.add('show');
    $img.classList.remove('move-in');
    $img.classList.add('move-out');
  };

  vm.galleryPrev = function(galleryPics , galleryHref){//directive
    galleryStartIndex = galleryStartIndex - 1;
    if(galleryPics[galleryStartIndex] == undefined) {
      galleryStartIndex = galleryPics.length - 1;
    }
    var hrefImg = "assets/img/gallery/"+ galleryHref +"/" + galleryPics[galleryStartIndex][0];
    var $img = document.getElementById('gallerySliderImg');
    var $imgLoader = document.getElementsByClassName('gs-img-loader')[0];
    setTimeout(function(){
      $img.src = hrefImg;
    },350);
    $imgLoader.classList.add('show');
    $img.classList.remove('move-in');
    $img.classList.add('move-out');
  };

  vm.galleryClose = function(){//directive
    var $img = document.getElementById('gallerySliderImg');
    var $body = document.getElementsByTagName('body')[0];
    var $gallerySlider = document.getElementById('gallerySlider');

    $gallerySlider.classList.remove('show');
    $body.classList.remove('no-scroll');
    $img.classList.remove('move-in');
    $img.classList.remove('move-out');
    firstLoadImg = false;
  };

  var $gallerySlider = document.getElementById('gallerySlider');
  if($gallerySlider){
    var hammer    = new Hammer.Manager($gallerySlider);
    var swipe     = new Hammer.Swipe();
    hammer.add(swipe);
    hammer.on('swipeleft', function(){
      vm.galleryNext(galleryPicsStore, galleryHrefStore);
    });
    hammer.on('swiperight', function(){
      vm.galleryPrev(galleryPicsStore, galleryHrefStore);
    });
  }

  // GALLERY SLIDER IN ARTICLE -> END


};//articlesCtrl


var galleryMobile650 = function(pic, galleryPics, galleryHref, imgIndex) {
  var $imgs = document.getElementsByClassName('a-img');
  var $gallery = document.getElementsByClassName('a-gallery')[0];
  var mobileGalActive = $gallery.classList.contains('mobile-gal-big') ? true : false ;

  if (mobileGalActive == false) {
    $gallery.classList.remove('mobile-gal-small');
    $gallery.classList.add('mobile-gal-big');
    var moveTo = $imgs[imgIndex] ? $imgs[imgIndex].offsetTop : $gallery.offsetTop - 50;
    Velocity(document.body, "scroll", {offset: moveTo , mobileHA: false, duration: 0});
  }else{
    $gallery.classList.remove('mobile-gal-big');
    $gallery.classList.add('mobile-gal-small');
    Velocity(document.body, "scroll", {offset: $gallery.offsetTop - 50 , mobileHA: false, duration: 0});
  }
}

var articleElementData = function(elData) {
  if(!elData) return false;
  if(elData == null || elData == false){
    return false;
  }else{
    return true;
  }
};

articles.directive('gallerySlider', ["$timeout", function($timeout){
  return{
    link: function(scope, element, attrs){

    }
  }
}]);


articles.directive('gallery', ["$timeout", function($timeout){ // NOT YET, but getting somewhere
  return{
    link: function(scope, element, attrs){
      if(scope.$first && scope.vm.articleData.mainPic) {
        element[0].style.display = "none";
      }
      if (scope.$last){
        $timeout(function() {
          element[0].children[0].addEventListener('load' , function(){
            window.eotm.thumbsContainerAnimate( true, 'galleryLoader');
            setTimeout(function(){
              window.eotm.thumbsContainerAnimate( true, 'galleryLoader');
            },5000);
          });
        },0);
      }
    }
  }
}]);

articles.directive('youtube', ["$window", function($window) {
  return {
    restrict: "E",
    priority: 1,
    scope: {
      videoid:  "@"
    },
    link: function(scope, element, attrs) {
      var createVideo = function(videoidScope){
        var player;
        player = new YT.Player(element.children()[0],
        {
          height: '100%',
          width: '100%',
          // maxWidth: '100%',
          videoId: scope.videoid,
          events: {
            'onReady': onPlayerReady,
          }
        });
      };

      if(scope.videoid === "null"){
        return;
      }else{
        var $scriptYotubeApi = document.getElementsByClassName('scriptYotubeApi')[0];
        if($scriptYotubeApi != undefined){
          createVideo();
        }else {
          var tag = document.createElement('script');
          tag.classList.add('scriptYotubeApi');
          tag.src = "https://www.youtube.com/iframe_api";
          var firstScriptTag = document.getElementsByTagName('script')[0];
          firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

          $window.onYouTubeIframeAPIReady = function() {
            createVideo();
          };
        }
      }
      function onPlayerReady(event) {
        window.eotm.thumbsContainerAnimate( true ,'videoAPI');
      }
    }
  }
}]);
;var menu = angular.module('eotmApp.menu', [
  'ngAnimate',

]);

//**********CONTROLLERS*******************************************

menu.service('menuData', ['$http' , menuData]);
menu.controller('menuCtrl', ['$scope','$stateParams','$state', '$rootScope', '$timeout', '$window' , '$document', 'menuData', '$filter' , menuCtrl]);

//***********************************
//MENU
// - menuData - service for loading menu data
//***********************************

function menuData($http) {
  var model = this;
  var URLS = {
    MENU: 'data/menu.json',
    ARTICLES: 'data/articles.json',
  };
  var menu;
  model.getMenuData = function() {
    return $http.get(URLS.MENU);
  };
  model.getArticlesData = function(){
    return $http.get(URLS.ARTICLES);;
  };
};


//*************************************
//MenuCTRL -
//*************************************


function menuCtrl($scope, $stateParams, $state, $rootScope, $timeout, $window, $document, menuData, $filter ){
  var vm = this;

  menuData.getArticlesData().then(function(result){
    vm.articlesAll = result.data;
    vm.articles = result.data;
    vm.search.tags = [];
    result.data.forEach(function(a){
      if(!a.tags) return;
      a.tags.forEach(function(tag){
        if(!vm.search.tags.includes(tag)) vm.search.tags.push(tag);
      })
    })
  });

  menuData.getMenuData().then(function(result){
    vm.menuData = result.data;
    vm.subMenuData = result.data;
  });

  vm.menuHiddenFirst = false;
  vm.menuShow = true;

  vm.menuMobile = "menu/small.tmpl.html";
  vm.menuSearch = "menu/search/search.tmpl.html";

  vm.triGSvg = "assets/img/common/tri-g.svg";
  vm.triPSvg = "assets/img/common/tri-p.svg";
  vm.menuLogoSvg = "assets/img/common/menu-logo.svg";
  vm.menuSwitchSvg = "assets/img/common/switch.svg";
  vm.menuMobileToggleSvg = "assets/img/common/mobile-menu-toggle.svg";

  vm.menuActive = { subm: null , main: null, mobileState: null};

  vm.menuMobileToggle = function(){

  }

  $scope.$on('menuChange', function(event, data){
    vm.menuActive.main = data.menuMain == '' ? null : data.menuMain ;
    vm.menuActive.subm = data.menuSub == '' ? null : data.menuSub;
    vm.menuActive.mobileState = data.menuMobileState;
    vm.subMenuData = vm.menuData.find(function(m){
      if(m.category == vm.menuActive.main)    return m;
    })
    refreshTags(data);
  })

  // vm.searchMenuPosition = 'bottom';
  vm.searchMenuPosition = 'side';

  //searchMenu START -> put in separate in search menu controller??
  var refreshTags = function(stateCurrent){

    vm.articles = $filter('filter')(vm.articlesAll, { "category": stateCurrent.menuMain, "subcategory": stateCurrent.menuSub});
    vm.search.tags = [];
    vm.search.tagsActive = [];
    vm.articles.forEach(function(a){
        if(!a.tags) return;
        a.tags.forEach(function(tag){
          if(!vm.search.tags.includes(tag)) vm.search.tags.push(tag);
        })
    })
  }

  vm.search = {tags: [], tagsActive: [], word: null, open: false, category: null, subCategory: null}
  vm.search.tagsActive = [];
  vm.searchTagChange = function(tag){
    if ( vm.search.tagsActive.includes(tag)) {
      var index = vm.search.tagsActive.indexOf(tag);
      if (index > -1) vm.search.tagsActive.splice(index, 1);
    }else{
      vm.search.tagsActive.push(tag);
    }
    $rootScope.$broadcast('searchMenu', vm.search);
  }
  vm.searchWordChange = function(){
    $rootScope.$broadcast('searchMenu', vm.search);
  }
  // implement broadcast on search word input change also;



    // vm.scrollAndMouseEvents = function() {
    //****************menu  event***************//

    $document.bind('scroll', function(){// directive
      var scrollNum = $window.pageYOffset;
      var setMenuShowTo = function(setTo){
        if(vm.menuShow != setTo){
          vm.menuShow = setTo;
          $scope.$apply();
        }
      }
      scrollNum < 150 ?  setMenuShowTo(true) :  setMenuShowTo(false);
    })// scrolll to directive



    // SWITCH TO DIRECTIVE
    var switchTo = function(colorNew, colorOld){ // directive
      $body.classList.add('ani-out-in');
      $scope.$parent.vm.eotmMoveIn = true;
      $timeout(function(){
        $scope.$parent.vm.eotmColor = colorNew;
        $body.classList.remove(colorOld);
        $body.classList.add(colorNew);
      },150);
      $timeout(function(){
        $scope.$parent.vm.eotmMoveIn = false;
        $body.classList.remove('ani-out-in');
      },700);
      localStorage.setItem('themeColor', colorNew);
    };

    var themeColor = localStorage.getItem('themeColor');
    var light = "rgb(229, 229, 229)";
    var dark = "rgb(60, 60, 60)";

    if (themeColor == null) {
      localStorage.setItem('themeColor', 'light');
      switchTo('light', 'dark');
    } else {
      themeColor = localStorage.getItem('themeColor');
      themeColor == 'light' ? switchTo('light', 'dark') : switchTo('dark', 'light');
    }

    vm.checkLocalAndSwitch = function(){
      themeColor = localStorage.getItem('themeColor');
      themeColor == 'dark' ? switchTo('light', 'dark') : switchTo('dark', 'light');
    };
    // SWITCH TO DIRECTIVE

};
;angular.module('eotmApp.templates', []).run(['$templateCache', function($templateCache) {$templateCache.put('eotmMain.tmpl.html','<div class="" ng-include="vm.menuTmpl"></div><div ui-view="article" class="view-animate" id="articleView"></div><div class="" ng-include="\'thumbs/thumbs.tmpl.html\'"></div>');
$templateCache.put('about/about.tmpl.html','<div id="article" class="loading"><div class="article-wrapper about-wrapper"><h2 class="a-head"><span>About</span></h2><p class="a-describe">Equations Of The Matrix is an platform for sharing knowledge, inspiration, works, projects and events.</p><p class="a-describe">Purpose of EOTM.info is to help One recognize the inner strength, accept One\'s individuality, connection to the world around us. To break the pattern of being an victim of circumstances and become a creator of One\'s life.</p><p class="a-describe">Our goal is to spread and share interesting documentaries and lectures and create new art project. We are currently also working on creating products which will help spreading the message of EOTM.INFO.</p><br><p class="a-describe">Don\'t hesitate to send us interesting links to share, lectures, films, documentaries, artists, paintings, graphics or photos.</p><p class="a-describe">For all inquires please send email to: <strong>hello@eotm.info </strong>or contact us on our social:</p><div class="social-wrapper"><div class="social-icon"><a title="Pinterest" href="https://www.pinterest.com/eotminfo/" target="_blank"><i class="fab fa-pinterest-p"></i></a></div><div class="social-icon"><a title="Youtube" href="http://www.youtube.com/channel/UCjxUu6OP3rC_gk9PUHd-fgA" target="_blank" class="social-icon"><i class="fab fa-youtube"></i></a></div><div class="social-icon"><a title="Facebook" href="https://www.facebook.com/eotm.info" target="_blank" class="social-icon"><i class="fab fa-facebook-f"></i></a></div><div class="social-icon"><a title="Instagram" href="https://www.instagram.com/eotm.info/" target="_blank" class="social-icon"><i class="fab fa-instagram"></i></a></div></div></div></div>');
$templateCache.put('articles/article.tmpl.html','<div class="loading" id="article" article-loader><div class="{{vm.articleData.href}} article-wrapper"><h3 class="a-head" ng-ig="vm.articleElementData(vm.articleData.headline)"><span>{{vm.articleData.headline}}</span><div class="a-shop-link" ng-if="vm.articleElementData(vm.articleData.shopLink)"><a href="{{vm.articleData.shopLink}}" target="_blank"><i class="fas fa-shopping-cart"></i></a></div></h3><div class="article-txt-wrapper"><div class="a-describe" ng-if="vm.articleElementData(vm.articleData.description)">{{vm.articleData.description}}</div><div class="a-vid-desc" ng-if="vm.articleElementData(vm.articleData.vidDesc)">{{vm.articleData.vidDesc}}</div><div class="a-vid" ng-if="vm.articleElementData(vm.articleData.vidLink)"><youtube videoid="{{vm.articleData.vidLink}}"><div class="" id="player"></div></youtube></div><div class="a-book-review" ng-if="vm.articleElementData(vm.articleData.bookReview)"><p>{{vm.articleData.bookReview}}</p></div></div><div class="a-gallery" ng-if="vm.articleElementData(vm.articleData.gallery)"><div class="a-main-pic" ng-if="vm.articleElementData(vm.articleData.mainPic)"><img class="" ng-src="assets/img/gallery/{{vm.articleData.href}}/{{vm.articleData.gallery[0][1]}}" alt="" ng-click="vm.gallery(vm.articleData.gallery[0], vm.articleData.gallery, vm.articleData.href)" img-loader></div><div class="a-img" ng-click="vm.gallery(pic, vm.articleData.gallery, vm.articleData.href, $index)" ng-repeat="pic in vm.articleData.gallery" gallery><img class="" ng-src="assets/img/gallery/{{vm.articleData.href}}/{{pic[1]}}" alt="" img-loader></div></div><div class="a-source" ng-if="vm.articleElementData(vm.articleData.source)"><p><strong>Source: </strong><a href="{{vm.articleData.source}}" target="_blank"><span class="a-source-link">{{vm.articleData.source}}</span></a></p></div></div><div id="gallerySlider" class="" gallery-slider><div class="gallery-overlay" ng-click="vm.galleryClose()"></div><div class="gs-close" ng-click="vm.galleryClose()"><svg width="51" height="51" viewBox="0 0 51 51" id="menuToggle-btn" version="1.1"><g id="g4918" transform="matrix(0.95237101,0,0,0.95237101,0.90582575,1.2989051)"><circle r="24.666214" cy="25.391947" cx="25.824152" class="circle" style="fill:none;fill-opacity:1;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1"></circle><rect ry="1.6656303" y="13.650247" x="12.910566" height="3.3312607" width="25.523808" class="rect one" style="fill-opacity:1;stroke:none;stroke-width:1.48667049;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1"></rect><rect ry="1.6656303" y="23.815605" x="12.833264" height="3.3312607" width="25.523808" class="rect two" style="fill-opacity:1;stroke:none;stroke-width:1.48667049;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1"></rect><rect ry="1.6656303" y="33.900864" x="12.680457" height="3.3312607" width="25.523808" class="rect three" style="fill-opacity:1;stroke:none;stroke-width:1.48667049;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1"></rect></g></svg></div><div class="gs-img-wrapper"><div class="next gs-btn" ng-click="vm.galleryNext(vm.articleData.gallery, vm.articleData.href)"><img class="" src="assets/img/common/next-btn.png" alt=""> <img class="btn-2" src="assets/img/common/next-btn.png" alt=""></div><img id="gallerySliderImg" src="" alt=""><div class="gs-img-loader"><svg width="371" height="340" id="loaderMainSvg" version="1.1"><g inkscape:label="Layer 1" inkscape:groupmode="layer" id="layer1" transform="translate(0,-790.36218)"><g transform="matrix(1.2775126,0,0,1.2775126,-281.22424,24.867882)" id=""><g style="stroke-miterlimit:4;stroke-dasharray:none" id="logo" transform="matrix(0.62327102,0,0,0.62327102,645.43592,851.14872)"><g transform="matrix(0.49259348,0,0,0.48717968,-618.73128,-481.94963)" style="fill:none;stroke:#404040;stroke-width:8.18793106;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" id="s-circles"><path transform="matrix(0.5077783,0.87949781,-0.76377433,0.44096532,815.42467,149.53184)" style="stroke-miterlimit:4;stroke-dasharray:none" d="m -81.822359,539.20471 c 0,10.59995 -7.462301,19.1929 -16.667517,19.1929 -9.205214,0 -16.667514,-8.59295 -16.667514,-19.1929 0,-10.59994 7.4623,-19.1929 16.667514,-19.1929 9.205216,0 16.667517,8.59296 16.667517,19.1929 z" id="" class="s-circle" inkscape:connector-curvature="0"/><path transform="matrix(0.5077783,0.87949781,-0.76377433,0.44096532,970.88336,419.36003)" style="stroke-miterlimit:4;stroke-dasharray:none" d="m -81.822359,539.20471 c 0,10.59995 -7.462301,19.1929 -16.667517,19.1929 -9.205214,0 -16.667514,-8.59295 -16.667514,-19.1929 0,-10.59994 7.4623,-19.1929 16.667514,-19.1929 9.205216,0 16.667517,8.59296 16.667517,19.1929 z" id="path4407-4-0-361-4" class="s-circle" inkscape:connector-curvature="0"/><path transform="matrix(0.5077783,0.87949781,-0.76377433,0.44096532,664.29894,420.52449)" style="stroke-miterlimit:4;stroke-dasharray:none" d="m -81.822359,539.20471 c 0,10.59995 -7.462301,19.1929 -16.667517,19.1929 -9.205214,0 -16.667514,-8.59295 -16.667514,-19.1929 0,-10.59994 7.4623,-19.1929 16.667514,-19.1929 9.205216,0 16.667517,8.59296 16.667517,19.1929 z" id="path4407-0-7-3-7" class="s-circle" inkscape:connector-curvature="0"/></g><path style="stroke-miterlimit:4;stroke-dasharray:none" d="m -487.25674,-289.88495 84.8094,0 0,81.61369 -84.8094,0 z" id="rect" inkscape:connector-curvature="0"/></g></g></g></svg></div><div class="prev gs-btn" ng-click="vm.galleryPrev(vm.articleData.gallery, vm.articleData.href)"><img src="assets/img/common/prev-btn.png" alt=""> <img class="btn-2" src="assets/img/common/prev-btn.png" alt=""></div></div></div><!-- gallery Slider --><!-- article -->');
$templateCache.put('products/products.tmpl.html','<div id="article" class="loading"><div class="article-wrapper products-wrapper"><h2 class="a-head"><span>Products</span></h2><p class="a-describe"><span>We are currently working on our products. We have prepared PDF files ready to print for you in our shop </span><a class="shop-link-icon" href="http://shop.eotm.info" target="_blank"><i class="fas fa-shopping-cart"></i></a><br><span>Let us know what you think!</span></p><div class="a-gallery"><div class="a-img" ng-click="vm.gallery(pic, vm.productsData[0].gallery, vm.productsData[0].href)" ng-repeat="pic in vm.productsData[0].gallery" gallery-loader><img class="" ng-src="assets/img/gallery/products/{{pic[1]}}" alt="" img-loader></div></div><!-- gallery --><div class="shop-btn-wrapper"><a class="" href="http://shop.eotm.info" target="_blank"><span>Checkout our shop </span><i class="fas fa-shopping-cart"></i></a></div><div id="gallerySlider" class=""><div class="gallery-overlay" ng-click="vm.galleryClose()"></div><div class="gs-close" ng-click="vm.galleryClose()"><svg width="51" height="51" viewBox="0 0 51 51" id="menuToggle-btn" version="1.1"><g id="g4918" transform="matrix(0.95237101,0,0,0.95237101,0.90582575,1.2989051)"><circle r="24.666214" cy="25.391947" cx="25.824152" class="circle" style="fill:none;fill-opacity:1;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1"></circle><rect ry="1.6656303" y="13.650247" x="12.910566" height="3.3312607" width="25.523808" class="rect one" style="fill-opacity:1;stroke:none;stroke-width:1.48667049;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1"></rect><rect ry="1.6656303" y="23.815605" x="12.833264" height="3.3312607" width="25.523808" class="rect two" style="fill-opacity:1;stroke:none;stroke-width:1.48667049;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1"></rect><rect ry="1.6656303" y="33.900864" x="12.680457" height="3.3312607" width="25.523808" class="rect three" style="fill-opacity:1;stroke:none;stroke-width:1.48667049;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1"></rect></g></svg></div><div class="gs-img-wrapper"><div class="next gs-btn" ng-click="vm.galleryNext(vm.productsData[0].gallery, vm.productsData[0].href)"><img src="assets/img/common/next-btn.png" alt=""> <img class="btn-2" src="assets/img/common/next-btn.png" alt=""></div><img id="gallerySliderImg" src="" alt=""><div class="gs-img-loader"><svg width="371" height="340" id="loaderMainSvg" version="1.1"><g inkscape:label="Layer 1" inkscape:groupmode="layer" id="layer1" transform="translate(0,-790.36218)"><g transform="matrix(1.2775126,0,0,1.2775126,-281.22424,24.867882)" id=""><g style="stroke-miterlimit:4;stroke-dasharray:none" id="logo" transform="matrix(0.62327102,0,0,0.62327102,645.43592,851.14872)"><g transform="matrix(0.49259348,0,0,0.48717968,-618.73128,-481.94963)" style="fill:none;stroke:#404040;stroke-width:8.18793106;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" id="s-circles"><path transform="matrix(0.5077783,0.87949781,-0.76377433,0.44096532,815.42467,149.53184)" style="stroke-miterlimit:4;stroke-dasharray:none" d="m -81.822359,539.20471 c 0,10.59995 -7.462301,19.1929 -16.667517,19.1929 -9.205214,0 -16.667514,-8.59295 -16.667514,-19.1929 0,-10.59994 7.4623,-19.1929 16.667514,-19.1929 9.205216,0 16.667517,8.59296 16.667517,19.1929 z" id="" class="s-circle" inkscape:connector-curvature="0"/><path transform="matrix(0.5077783,0.87949781,-0.76377433,0.44096532,970.88336,419.36003)" style="stroke-miterlimit:4;stroke-dasharray:none" d="m -81.822359,539.20471 c 0,10.59995 -7.462301,19.1929 -16.667517,19.1929 -9.205214,0 -16.667514,-8.59295 -16.667514,-19.1929 0,-10.59994 7.4623,-19.1929 16.667514,-19.1929 9.205216,0 16.667517,8.59296 16.667517,19.1929 z" id="path4407-4-0-361-4" class="s-circle" inkscape:connector-curvature="0"/><path transform="matrix(0.5077783,0.87949781,-0.76377433,0.44096532,664.29894,420.52449)" style="stroke-miterlimit:4;stroke-dasharray:none" d="m -81.822359,539.20471 c 0,10.59995 -7.462301,19.1929 -16.667517,19.1929 -9.205214,0 -16.667514,-8.59295 -16.667514,-19.1929 0,-10.59994 7.4623,-19.1929 16.667514,-19.1929 9.205216,0 16.667517,8.59296 16.667517,19.1929 z" id="path4407-0-7-3-7" class="s-circle" inkscape:connector-curvature="0"/></g><path style="stroke-miterlimit:4;stroke-dasharray:none" d="m -487.25674,-289.88495 84.8094,0 0,81.61369 -84.8094,0 z" id="rect" inkscape:connector-curvature="0"/></g></g></g></svg></div><div class="prev gs-btn" ng-click="vm.galleryPrev(vm.productsData[0].gallery, vm.productsData[0].href)"><img src="assets/img/common/prev-btn.png" alt=""> <img class="btn-2" src="assets/img/common/prev-btn.png" alt=""></div></div></div><!-- gallery Slider --></div></div>');
$templateCache.put('menu/menu.tmpl.html','<div class="" ng-controller="menuCtrl as vm"><div id="menu" class="" ng-class="{\'hidden\': vm.menuShow == false , \'hidden-first\': vm.menuHiddenFirst }  " ng-mouseover="vm.menuShow = true"><div class="menu-wrapper"><!-- <div class="menu-hidden-container">\n        <span class="tri-wrapper" ng-include="vm.triGSvg"> </span>\n        <span class="tri-wrapper" ng-include="vm.triPSvg"> </span>\n        <span class="tri-wrapper" ng-include="vm.triGSvg"> </span>\n        <span class="tri-wrapper" ng-include="vm.triPSvg"> </span>\n      </div> --><div class="menu-logo" ui-sref="home"><div class="" ng-include="vm.menuLogoSvg"></div></div><div class="menu-tabs-wrapper"><div ng-repeat="m in vm.menuData" class="menu-tab {{m.category}}" ui-sref="{{m.category}}" ng-class="{ \'active\': vm.menuActive.main == m.category}"><span class="menu-txt">{{m.category}}</span></div></div><div class="social-wrapper search-btn-wrapper"><div class="search-btn" ng-click="vm.search.open = !vm.search.open"><span class="txt">Search</span> <i class="fas fa-search"></i></div></div><div class="" id="switch" ng-click="vm.checkLocalAndSwitch()"><span class="switch-txt">Switch&nbsp;color</span><div class="switch-wrapper" ng-include="vm.menuSwitchSvg"></div></div><div class="sub-menu-wrapper" ng-class="{\'empty\' : !vm.subMenuData}"><div class="sub-menu {{sub}}" ng-repeat="sub in vm.subMenuData.subcategories" ui-sref="{{vm.subMenuData.category}}.category({ category : \'{{sub}}\' })" ng-class="{ \'active\': vm.menuActive.subm == sub}">{{sub}}</div></div></div></div><div class="pseudo-menu"></div><div class="" ng-include="vm.menuMobile"></div><div class="" ng-include="vm.menuSearch"></div></div>');
$templateCache.put('menu/small.tmpl.html','<div id="menuHeaderWrapper"><div class="logo" ui-sref="home"></div><span class="toggle-txt" id="menuMobileState">{{vm.menuActive.mobileState}}</span><div class="search-btn-wrapper"><div class="search-btn" ng-click="vm.search.open = !vm.search.open"><span class="txt">Search</span> <i class="fas fa-search"></i></div></div><div class="menu-close" id="menuMobileToggle" ng-click="vm.menuMobileToggle()"><div class="" ng-include="vm.menuMobileToggleSvg"></div></div></div><div id="menuMobile"><div class="m-mob-wrapper"><div class="logo"></div><div class="m-mob-tabs-wrapper"><div ng-repeat="m in vm.menu" class="m-mob-tab {{m.category}}"><span class="menu-txt" ui-sref="{{m.category}}">{{m.category}}</span><div class="sub-menu-tab" ng-repeat="subm in m.subcategories" ui-sref="{{m.category}}.category({ category : \'{{subm}}\' })">{{subm}}</div></div></div><div class="social-wrapper"><div class="social-icon"><a title="Pinterest" href="https://www.pinterest.com/eotminfo/" target="_blank"><i class="fab fa-pinterest-p"></i></a></div><div class="social-icon"><a title="Youtube" href="http://www.youtube.com/channel/UCjxUu6OP3rC_gk9PUHd-fgA" target="_blank" class="social-icon"><i class="fab fa-youtube"></i></a></div><div class="social-icon"><a title="Facebook" href="https://www.facebook.com/eotm.info" target="_blank" class="social-icon"><i class="fab fa-facebook-f"></i></a></div><div class="social-icon"><a title="Instagram" href="https://www.instagram.com/eotm.info/" target="_blank" class="social-icon"><i class="fab fa-instagram"></i></a></div></div><div class="" id="switchMobile" ng-click="vmcheckLocalAndSwitch()"><span class="switch-txt">Switch&nbsp;color</span><div class="switch-wrapper" ng-include="vm.menuSwitchSvg"></div></div></div></div>');
$templateCache.put('thumbs/thumbs.tmpl.html','<div class="" ng-controller="thumbsCtrl as vm"><ul class="thumbs-container" ng-class="{ \'appear\': vm.thumbsAppear }"><li class="thumb" ng-repeat="thumb in vm.thumbs | filter: { category : vm.tFilterC , subcategory : vm.tFilterS  } | tagsFilter:vm.search.tagsActive | wordFilter:vm.search.word | limitTo : vm.loadMoreLimit" ng-click="vm.articleLoad(thumb.href, thumb.unique)"><div class="thumb-wrapper"><div class="thumb-img"><img ng-src="assets/img/thumbs/{{thumb.thumbPic}}" alt="" img-loader></div><div class="t-info-wrapper"><div class="t-head">{{thumb.headline}}</div><div class="t-category"><span>{{thumb.subcategory}}</span></div><div class="t-desc">{{thumb.thumbDesc}}</div><!-- <div ng-if="vm.articleElementData(thumb.artist)" class="t-artist"><span>Arts by: </span>{{thumb.artist}}</div> --><!-- <div class="t-date">{{thumb.date}}</div> --></div></div></li></ul><div id="loadMoreBtn" load-more-thumbs></div></div>');
$templateCache.put('articles/unique/breath.html','<div class="breath-wrapper" ng-controller="breathPageCtrl as vm"><audio id="audio" controls audio-play><source src="app/articles/unique/code/breath/bell.mp3" type="audio/mpeg">Your browser does not support the audio element.</audio><span class="">{{vm.interval/1000}}s</span><div class="controls-wrapper"><span ng-click="vm.interval = vm.interval - 1000" style="cursor:pointer; font-size:35px; font-weight:600; padding: 20px">- </span><span ng-click="vm.interval = vm.interval + 1000" style="cursor:pointer; font-size:35px; font-weight:600; padding: 20px">+</span></div></div>');
$templateCache.put('articles/unique/dune1984.html','<div class="loading" id="article" article-loader><div class="{{vm.articleData.href}} article-wrapper"><h3 class="a-head"><span>Dune (1984)</span></h3><div class="article-txt-wrapper"><div class="a-describe">Dune is a science fiction film written and directed by David Lynch, based on the 1965 Frank Herbert novel.</div><div class="a-describe">Short scenes from science fiction movie Dune from 1984. One scene as a depiction of human controling animal instints and other a technology based on sound vibration</div><div class="a-describe" style="text-align:center">Duke\'s son undergoes the test of the box where he needs to control his instincts and overcome his fear and pain.</div><div class="a-vid"><iframe width="560" height="315" src="https://www.youtube.com/embed/0ujoXRAZU3g" frameborder="0" allowfullscreen></iframe></div><div class="a-describe" style="text-align:center">Duke\'s son shows a weapon which enhances the sound of a certain tought.</div><div class="a-vid"><iframe width="560" height="315" src="https://www.youtube.com/embed/ltgC6eOf_Gw" frameborder="0" allowfullscreen></iframe></div><div class="a-source"><p><strong>Source: </strong><a href="http://www.imdb.com/title/tt0087182/" target="_blank"><span class="a-source-link">http://www.imdb.com</span></a></p></div></div></div></div><!-- article -->');
$templateCache.put('articles/unique/thegreatwork.html','<div class=""><div class="container"><h2 class="video-title">The Great Work</h2><p>The Science of the Secret. An Initiate\'s guide threw the doors of Esoteric Knowledge...<br>10 years in the making, The Great Work is a Modern Day Mystery School designed to help you meet the challenges and opportunities of life in the 21st century.<br></p><h3 class="video-description">Prologue</h3><div class="video"><iframe width="560" height="315" src="https://www.youtube.com/embed/WOjPyNLvpA4" frameborder="0" allowfullscreen></iframe></div><h3 class="video-description">The Material Plane</h3><div class="video"><iframe width="560" height="315" src="https://www.youtube.com/embed/hr5BwTVPYwA" frameborder="0" allowfullscreen></iframe></div><h3 class="video-description">The Mental Plane</h3><div class="video"><iframe width="560" height="315" src="https://www.youtube.com/embed/83oTRusr7LU" frameborder="0" allowfullscreen></iframe></div><h3 class="video-description">The Emotional Plane</h3><div class="video"><iframe width="560" height="315" src="https://www.youtube.com/embed/JDRTZ1ibpPk" frameborder="0" allowfullscreen></iframe></div><h3 class="video-description">The Plane Of Will</h3><div class="video"><iframe width="560" height="315" src="https://www.youtube.com/embed/zwMiqwwl-gQ" frameborder="0" allowfullscreen></iframe></div><h3 class="video-description">The Quintessence</h3><div class="video"><iframe width="560" height="315" src="https://www.youtube.com/embed/7MJZLuwwX9E" frameborder="0" allowfullscreen></iframe></div></div></div>');
$templateCache.put('menu/search/search.tmpl.html','<div class="" id="searchMenu" ng-class=" {\'show\' : vm.search.open } "><div class="search-menu-wrapper"><span class="search-close-btn" ng-click="vm.search.open = !vm.search.open">X</span><h3 class="search-h"><span class="upper head">Search</span><span ng-if="vm.menuActive.main"> in <span class="upper">{{vm.menuActive.main}}</span><span ng-if="vm.menuActive.subm"> under <span class="upper">{{vm.menuActive.subm}}</span></span></span></h3><div class="search-word"><h5 class="tags-h">Search by word</h5><div class="form-group"><label for="searchWord" ng-if="!vm.search.word">Search by word</label><input id="searchWord" type="text" name="" value="" placeholder="" ng-model="vm.search.word" ng-change="vm.searchWordChange()"></div><div class="input-clear" ng-click="vm.search.word = null" ng-if="vm.search.word"><span>clear</span></div></div><div class="tags-wrapper"><span class="clear-tags" ng-click="vm.search.tagsActive = []" ng-if="vm.search.tagsActive.length > 0">clear tags</span><div class="pick-tags-wrapper"><h5 class="tags-h">Pick from tags</h5><div class="tag" ng-repeat="tag in vm.search.tags" ng-click="vm.searchTagChange(tag)" ng-class="{\'active\' : vm.search.tagsActive.includes(tag)}">{{tag}}</div></div></div></div></div>');}]);;var thumbs = angular.module('eotmApp.thumbs', [

]);

thumbs.service('thumbsData',['$http' , thumbsData]);
thumbs.controller('thumbsCtrl', ['$scope','thumbsData','$stateParams','$state', '$rootScope', '$timeout' , thumbsCtrl]);


function thumbsData($http) {
  var model = this;
  var URLS = {
    FETCH: 'data/articles.json'
  };
  var thumbs;
  model.getThumbsData = function() {
    return $http.get(URLS.FETCH);
  };
};

function thumbsCtrl($scope, thumbsData, $stateParams, $state, $rootScope, $timeout ){
  var vm = this;
  thumbsData.getThumbsData().then(function(result){
    vm.thumbs = result.data;
    vm.thumbsAll = result.data;
  });

  vm.loadMoreLimit = 12; // set to 6 in mobile? no need
  vm.tFilterC;
  vm.tFilterS;
  vm.thumbsAppear = false;
  vm.search = null;

  $scope.$watch($scope.$parent.vm.firstLoadOn,function() {
    vm.thumbsAppear = true;
  })

  $scope.$on('menuChange', function(event, data){
    vm.tFilterC = data.menuMain;
    vm.tFilterS = data.menuSub;
  })

  $scope.$on('searchMenu', function(event, data){
    vm.search = data;
  })

  vm.articleLoad = function (articleHref, isUnique) {
    var timeOutArticleLoad;
    if(window.scrollY != 0){
      timeOutArticleLoad = 350;
    }else{
      timeOutArticleLoad = 0;
    }
    setTimeout(function(){
      if(isUnique == true){
        $state.go('unique', {unique : articleHref});
      }else{
        $state.go('article', {article : articleHref});
      }
    },timeOutArticleLoad - 150);
  };

};

thumbs.filter('tagsFilter', function() {

  return function(input, searchTags) {
    var thumbsAll = input;
    var thumbsFiltered = [];
    if(thumbsAll && searchTags && searchTags.length > 0){

      thumbsAll.forEach(function(i){
        i.tags.forEach(function(tag){
          if(searchTags.includes(tag)){
            if(!thumbsFiltered.includes(i)){
              thumbsFiltered.push(i);
            }
          }
        });
      });
    }else{
      thumbsFiltered = thumbsAll;
    }
    return thumbsFiltered;
  }
});

thumbs.filter('wordFilter', function() {

  return function(input, searchWord) {
    var searchFields = [ 'headline', 'thumbDesc', 'description', 'artist','vidDesc', 'vidLink', 'source' ];
    if(!searchWord) return input;

    var articlesAll = input;
    var articlesFiltered = [];
      articlesAll.forEach(function(a){
        searchFields.forEach(function(f){
          if(!a[f]) return;
          var fieldContent = a[f].toLowerCase();
          if(fieldContent.includes(searchWord.toLowerCase())){
            if(!articlesFiltered.includes(a)) articlesFiltered.push(a);
          }
        })
      });

    return articlesFiltered;
  }
});


thumbs.directive("loadMoreThumbs", ["$window", "$document", function ($window, $document) { // scroll listener for LOADMOREE
return{
  link: function(scope, element, attrs) {
    var bodyHeight = document.body.scrollHeight;
    var moreIndex = 12;
    var loadMore = function() {
      scope.vm.loadMoreLimit += moreIndex;
    };
    angular.element($window).bind("scroll", function() {
      bodyHeight = document.body.scrollHeight;
      if ($window.pageYOffset + $window.innerHeight + 50 > bodyHeight){
        loadMore();
        scope.$apply();
      }
    });
  }
}
}]);
;;var breath = angular.module('eotmApp.articles.breath', [
  'ui.router',
  'ngAnimate'
]);

//**********CONTROLLERS*******************************************

breath.controller('breathPageCtrl', ['$scope', '$document' ,'$stateParams','$filter' , '$timeout', breathPageCtrl]);


//*************************************
//breathPageCtrl CTRL
//*************************************

function breathPageCtrl($scope, $document ,$stateParams, $filter, $timeout){
  var vm = this;

  vm.interval = 10000;

};//BreathCtrl

eotmApp.directive('audioPlay', function(){
  return{
    link: function(scope, element, attrs){

     function play(){
       if(scope.vm.interval < 1001) scope.vm.interval = 2000;
        setTimeout(function(){
          element[0].pause();
          element[0].currentTime = 0;
          element[0].play();
          play();
        }, scope.vm.interval);

      }

      play();

      // chameleon nice gradients
      // https://codepen.io/teddyjefferson/pen/eNqKEP

    }
  }
});
