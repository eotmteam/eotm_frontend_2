var menu = angular.module('eotmApp.menu', [
  'ngAnimate',

]);

//**********CONTROLLERS*******************************************

menu.service('menuData', ['$http' , menuData]);
menu.controller('menuCtrl', ['$scope','$stateParams','$state', '$rootScope', '$timeout', '$window' , '$document', 'menuData', '$filter' , menuCtrl]);

//***********************************
//MENU
// - menuData - service for loading menu data
//***********************************

function menuData($http) {
  var model = this;
  var URLS = {
    MENU: 'data/menu.json',
    ARTICLES: 'data/articles.json',
  };
  var menu;
  model.getMenuData = function() {
    return $http.get(URLS.MENU);
  };
  model.getArticlesData = function(){
    return $http.get(URLS.ARTICLES);;
  };
};


//*************************************
//MenuCTRL -
//*************************************


function menuCtrl($scope, $stateParams, $state, $rootScope, $timeout, $window, $document, menuData, $filter ){
  var vm = this;

  menuData.getArticlesData().then(function(result){
    vm.articlesAll = result.data;
    vm.articles = result.data;
    vm.search.tags = [];
    result.data.forEach(function(a){
      if(!a.tags) return;
      a.tags.forEach(function(tag){
        if(!vm.search.tags.includes(tag)) vm.search.tags.push(tag);
      })
    })
  });

  menuData.getMenuData().then(function(result){
    vm.menuData = result.data;
    vm.subMenuData = result.data;
  });

  vm.menuHiddenFirst = false;
  vm.menuShow = true;

  vm.menuMobile = "menu/small.tmpl.html";
  vm.menuSearch = "menu/search/search.tmpl.html";

  vm.triGSvg = "assets/img/common/tri-g.svg";
  vm.triPSvg = "assets/img/common/tri-p.svg";
  vm.menuLogoSvg = "assets/img/common/menu-logo.svg";
  vm.menuSwitchSvg = "assets/img/common/switch.svg";
  vm.menuMobileToggleSvg = "assets/img/common/mobile-menu-toggle.svg";

  vm.menuActive = { subm: null , main: null, mobileState: null};

  vm.menuMobileToggle = function(){

  }

  $scope.$on('menuChange', function(event, data){
    vm.menuActive.main = data.menuMain == '' ? null : data.menuMain ;
    vm.menuActive.subm = data.menuSub == '' ? null : data.menuSub;
    vm.menuActive.mobileState = data.menuMobileState;
    vm.subMenuData = vm.menuData.find(function(m){
      if(m.category == vm.menuActive.main)    return m;
    })
    refreshTags(data);
  })

  // vm.searchMenuPosition = 'bottom';
  vm.searchMenuPosition = 'side';

  //searchMenu START -> put in separate in search menu controller??
  var refreshTags = function(stateCurrent){

    vm.articles = $filter('filter')(vm.articlesAll, { "category": stateCurrent.menuMain, "subcategory": stateCurrent.menuSub});
    vm.search.tags = [];
    vm.search.tagsActive = [];
    vm.articles.forEach(function(a){
        if(!a.tags) return;
        a.tags.forEach(function(tag){
          if(!vm.search.tags.includes(tag)) vm.search.tags.push(tag);
        })
    })
  }

  vm.search = {tags: [], tagsActive: [], word: null, open: false, category: null, subCategory: null}
  vm.search.tagsActive = [];
  vm.searchTagChange = function(tag){
    if ( vm.search.tagsActive.includes(tag)) {
      var index = vm.search.tagsActive.indexOf(tag);
      if (index > -1) vm.search.tagsActive.splice(index, 1);
    }else{
      vm.search.tagsActive.push(tag);
    }
    $rootScope.$broadcast('searchMenu', vm.search);
  }
  vm.searchWordChange = function(){
    $rootScope.$broadcast('searchMenu', vm.search);
  }
  // implement broadcast on search word input change also;



    // vm.scrollAndMouseEvents = function() {
    //****************menu  event***************//

    $document.bind('scroll', function(){// directive
      var scrollNum = $window.pageYOffset;
      var setMenuShowTo = function(setTo){
        if(vm.menuShow != setTo){
          vm.menuShow = setTo;
          $scope.$apply();
        }
      }
      scrollNum < 150 ?  setMenuShowTo(true) :  setMenuShowTo(false);
    })// scrolll to directive



    // SWITCH TO DIRECTIVE
    var switchTo = function(colorNew, colorOld){ // directive
      $body.classList.add('ani-out-in');
      $scope.$parent.vm.eotmMoveIn = true;
      $timeout(function(){
        $scope.$parent.vm.eotmColor = colorNew;
        $body.classList.remove(colorOld);
        $body.classList.add(colorNew);
      },150);
      $timeout(function(){
        $scope.$parent.vm.eotmMoveIn = false;
        $body.classList.remove('ani-out-in');
      },700);
      localStorage.setItem('themeColor', colorNew);
    };

    var themeColor = localStorage.getItem('themeColor');
    var light = "rgb(229, 229, 229)";
    var dark = "rgb(60, 60, 60)";

    if (themeColor == null) {
      localStorage.setItem('themeColor', 'light');
      switchTo('light', 'dark');
    } else {
      themeColor = localStorage.getItem('themeColor');
      themeColor == 'light' ? switchTo('light', 'dark') : switchTo('dark', 'light');
    }

    vm.checkLocalAndSwitch = function(){
      themeColor = localStorage.getItem('themeColor');
      themeColor == 'dark' ? switchTo('light', 'dark') : switchTo('dark', 'light');
    };
    // SWITCH TO DIRECTIVE

};
