var eotmApp = angular.module('eotmApp', [
  'ui.router',
  'ngAnimate',
  'eotmApp.articles',
  'eotmApp.thumbs',
  'eotmApp.menu',
  'eotmApp.templates'
  // 'eotmApp.menuSearch',
  // 'eotmApp.articles.breath'
]);

eotmApp.config(function($stateProvider){
  $stateProvider
  .state('about', {
    url: '/about',
    views: {
      'article': {
        templateUrl: 'about/about.tmpl.html',
        controller: 'articlesCtrl as vm'
      }
    }
  })
  .state('products', {
    url: "/products",
    views: {
      'article': {
        templateUrl: "products/products.tmpl.html",
        controller: 'articlesCtrl as vm'
      }
    }
  })
  .state('article', {
    url: "/article/:article",
    views: {
      'article': {
        templateUrl: "articles/article.tmpl.html",
        controller: "articlesCtrl as vm"
      }
    }
  })
  .state('unique', {
    url: "/unique/:unique",
    views: {
      'article': {
        templateUrl: function(stateParams){return 'app/articles/unique/' + stateParams.unique  + '.html'; },
        controller: "articlesCtrl as vm"
      }
    }
  })
  .state('home', {
    url: "/"
  })
  .state('knowledge', {
    url: "/knowledge",
  })
  .state('knowledge.category', {
    url: "/:category"
  })
  .state('works', {
    url: "/works"
  })
  .state('works.category', {
    url: "/:category"
  })
});



//**********CONTROLLERS*******************************************
// eotmApp.service('menuData', ['$http' , menuData]);
eotmApp.controller('mainCtrl', ['$scope','$stateParams','$state', '$filter','menuData','$rootScope', '$timeout', '$window',  mainCtrl]);


//*************************************
//MAINCTRL -
//*************************************


function mainCtrl($scope , $stateParams , $state, $filter , menuData , $rootScope, $timeout, $window){
  var vm = this;

  // vm.eotmMainTmpl = "eotmMain.tmpl.html";
  vm.thumbsTmpl = "thumbs/thumbs.tmpl.html";
  vm.menuTmpl = "menu/menu.tmpl.html";

  vm.firstLoadOn = false;
  vm.eotmColor = 'light';
  vm.eotmMoveIn = false;

  vm.mainLoader = function() {
    // vm.firstLoadOn = true;
    vm.loaderHideClass = true;
    $timeout(function () {
      vm.loaderHide = true;
    },350);
    $timeout(function () {
      vm.firstLoadOn = true;
    },600);
  };

  //loader start at 2 sec
  var winLoader = {flag: false, counter: 0 , min: 3 , max: 8 };
  var loaderInt = setInterval(function(){
    winLoader.counter += 1 ;
    if( winLoader.counter > winLoader.min && winLoader.flag == true || winLoader.counter > winLoader.max){
      vm.mainLoader();
      clearInterval(loaderInt);
    }
  },500);

  $timeout(function(){
    winLoader.flag = true;
    $window.eotm.init();
  },250);


  vm.menuMobileState = '';
  // vm.filterReset = filterReset;

  $rootScope.$on('$viewContentLoaded', function(event) {

    $timeout(function() {
      window.eotm.thumbsContainerAnimate(false , 'viewContentLoaded');
    },   $state.current.views ?  1000 :  0 );

  });


  $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams){

    var menuMain = '';
    var menuSub = '';
    var menuMobileState = '';

    if (toParams.category == undefined) {
      if(toState.name == 'home' || toState.name == "products" || toState.name == "about" || toState.name == "article" || toState.name == 'unique'){
        menuMobileState = '';
      }else{
        menuMobileState = toState.name;
      }
      menuMain = toState.name == 'knowledge' ? toState.name : toState.name == 'works' ? toState.name : '';
    }else{
      menuMain =  toState.name.split('.')[0];
      menuSub = toParams.category;
      menuMobileState = toParams.category;
    }

    $rootScope.$broadcast('menuChange', { menuMain: menuMain, menuSub: menuSub, menuMobileState: menuMobileState } );

    if(toState.name == 'article' || toState.name == 'products' || toState.name == 'about'){
      window.pageYOffset > window.innerHeight*1.5 ?  scrollToTop(0) :  scrollToTop(750);
    }else{
      scrollToTop(0);
    }
  });

};


var scrollToTop = function(durationX) {
  // var $menu = document.getElementById('menu');
  var $top = document.getElementsByClassName('main-wrapper')[0];
  Velocity( $top, "scroll", {duration: durationX});
  // if($menu) $menu.classList.remove('hidden');
};

//*************************************
//DIRECTIVES
//*************************************

eotmApp.directive('imgLoader', function($timeout){
  return{
    link: function(scope, element, attrs){
      element[0].classList.add('loading');
      var loader = document.createElement("span");
      loader.classList.add('loader');
      element[0].parentNode.appendChild(loader);

      element.bind("load",function(e){
        element[0].parentNode.removeChild(loader);
        element[0].classList.remove('loading');
      });
    }
  }
})
