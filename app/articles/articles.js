var articles = angular.module('eotmApp.articles', [
  'ui.router',
  'ngAnimate',
  'eotmApp.articles.breath'
]);

//**********CONTROLLERS*******************************************

eotmApp.service('productsData',['$http' , productsData]);
articles.controller('articlesCtrl', ['$scope','thumbsData','$stateParams','$filter' , 'productsData', '$timeout', articlesCtrl]);

function productsData($http) {
  var model = this;
  var URLS = {
    FETCH: 'data/products.json'
  };
  var products;
  model.getProductsData = function() {
    return $http.get(URLS.FETCH);
  };
};

//*************************************
//ARTICLE CTRL
//*************************************

function articlesCtrl($scope, thumbsData, $stateParams, $filter, productsData, $timeout){
  var vm = this;

  productsData.getProductsData().then(function(result){
    vm.productsData =  result.data;
  });

  thumbsData.getThumbsData().then(function(result){
    var filteredData = result.data.filter(function( obj ) {
      return obj.href == $stateParams.article;
    });
    vm.articleData = filteredData[0];
  });

  vm.articleElementData = articleElementData;
  vm.galleryMobile650 = galleryMobile650;
  vm.hrefImg;

// GALLERY SLIDER IN ARTICLE -> START
  var galleryStartIndex;
  var galleryPicsStore;
  var galleryHrefStore;
  vm.gallery = function(pic, galleryPics, galleryHref, $index) { //directive
    galleryHrefStore = galleryHref;
    galleryPicsStore = galleryPics;

    if(window.innerWidth < 650){
      var imgIndex = $index;
      vm.galleryMobile650(pic, galleryPics, galleryHref, imgIndex);
      return;
    }

    var $body = document.getElementsByTagName('body')[0];
    var firstLoadImg = false;
    var $img = document.getElementById('gallerySliderImg');
    var $gallerySlider = document.getElementById('gallerySlider');
    var $imgLoader = document.getElementsByClassName('gs-img-loader')[0];

    galleryStartIndex = galleryPics.indexOf(pic);
    var hrefImg = "assets/img/gallery/"+ galleryHref +"/" + pic[0];
    vm.hrefImg = "assets/img/gallery/"+ galleryHref +"/" + pic[0];
    $img.src = hrefImg;

    $gallerySlider.classList.add('show');
    $imgLoader.classList.add('show');
    $body.classList.add('no-scroll');

    // put separate -> out of controller
      $img.addEventListener('load', function(){
        setTimeout(function(){
          if(firstLoadImg == true){
            $img.classList.add('move-in');
          }
          $img.classList.remove('move-out');
          $imgLoader.classList.remove('show');
        },350);
        firstLoadImg = true;
      });

    document.addEventListener( 'keyup', function( event ){
      if (event.which === 27){
        vm.galleryClose();
      }
      if (event.which === 39){
        vm.galleryNext(galleryPicsStore, galleryHrefStore);
      };
      if (event.which === 37) {
        vm.galleryPrev(galleryPicsStore, galleryHrefStore);
      };
    });
  };


  vm.galleryNext = function(galleryPics , galleryHref){//directive
    galleryStartIndex = galleryStartIndex + 1;
    if(galleryPics[galleryStartIndex] == undefined) {
      galleryStartIndex = 0;
    }
    var $img = document.getElementById('gallerySliderImg');
    var $imgLoader = document.getElementsByClassName('gs-img-loader')[0];
    var hrefImg = "assets/img/gallery/"+ galleryHref +"/" + galleryPics[galleryStartIndex][0];
    setTimeout(function(){
      $img.src = hrefImg;
    },350);
    $imgLoader.classList.add('show');
    $img.classList.remove('move-in');
    $img.classList.add('move-out');
  };

  vm.galleryPrev = function(galleryPics , galleryHref){//directive
    galleryStartIndex = galleryStartIndex - 1;
    if(galleryPics[galleryStartIndex] == undefined) {
      galleryStartIndex = galleryPics.length - 1;
    }
    var hrefImg = "assets/img/gallery/"+ galleryHref +"/" + galleryPics[galleryStartIndex][0];
    var $img = document.getElementById('gallerySliderImg');
    var $imgLoader = document.getElementsByClassName('gs-img-loader')[0];
    setTimeout(function(){
      $img.src = hrefImg;
    },350);
    $imgLoader.classList.add('show');
    $img.classList.remove('move-in');
    $img.classList.add('move-out');
  };

  vm.galleryClose = function(){//directive
    var $img = document.getElementById('gallerySliderImg');
    var $body = document.getElementsByTagName('body')[0];
    var $gallerySlider = document.getElementById('gallerySlider');

    $gallerySlider.classList.remove('show');
    $body.classList.remove('no-scroll');
    $img.classList.remove('move-in');
    $img.classList.remove('move-out');
    firstLoadImg = false;
  };

  var $gallerySlider = document.getElementById('gallerySlider');
  if($gallerySlider){
    var hammer    = new Hammer.Manager($gallerySlider);
    var swipe     = new Hammer.Swipe();
    hammer.add(swipe);
    hammer.on('swipeleft', function(){
      vm.galleryNext(galleryPicsStore, galleryHrefStore);
    });
    hammer.on('swiperight', function(){
      vm.galleryPrev(galleryPicsStore, galleryHrefStore);
    });
  }

  // GALLERY SLIDER IN ARTICLE -> END


};//articlesCtrl


var galleryMobile650 = function(pic, galleryPics, galleryHref, imgIndex) {
  var $imgs = document.getElementsByClassName('a-img');
  var $gallery = document.getElementsByClassName('a-gallery')[0];
  var mobileGalActive = $gallery.classList.contains('mobile-gal-big') ? true : false ;

  if (mobileGalActive == false) {
    $gallery.classList.remove('mobile-gal-small');
    $gallery.classList.add('mobile-gal-big');
    var moveTo = $imgs[imgIndex] ? $imgs[imgIndex].offsetTop : $gallery.offsetTop - 50;
    Velocity(document.body, "scroll", {offset: moveTo , mobileHA: false, duration: 0});
  }else{
    $gallery.classList.remove('mobile-gal-big');
    $gallery.classList.add('mobile-gal-small');
    Velocity(document.body, "scroll", {offset: $gallery.offsetTop - 50 , mobileHA: false, duration: 0});
  }
}

var articleElementData = function(elData) {
  if(!elData) return false;
  if(elData == null || elData == false){
    return false;
  }else{
    return true;
  }
};

articles.directive('gallerySlider', function($timeout){
  return{
    link: function(scope, element, attrs){

    }
  }
});


articles.directive('gallery', function($timeout){ // NOT YET, but getting somewhere
  return{
    link: function(scope, element, attrs){
      if(scope.$first && scope.vm.articleData.mainPic) {
        element[0].style.display = "none";
      }
      if (scope.$last){
        $timeout(function() {
          element[0].children[0].addEventListener('load' , function(){
            window.eotm.thumbsContainerAnimate( true, 'galleryLoader');
            setTimeout(function(){
              window.eotm.thumbsContainerAnimate( true, 'galleryLoader');
            },5000);
          });
        },0);
      }
    }
  }
});

articles.directive('youtube', function($window) {
  return {
    restrict: "E",
    priority: 1,
    scope: {
      videoid:  "@"
    },
    link: function(scope, element, attrs) {
      var createVideo = function(videoidScope){
        var player;
        player = new YT.Player(element.children()[0],
        {
          height: '100%',
          width: '100%',
          // maxWidth: '100%',
          videoId: scope.videoid,
          events: {
            'onReady': onPlayerReady,
          }
        });
      };

      if(scope.videoid === "null"){
        return;
      }else{
        var $scriptYotubeApi = document.getElementsByClassName('scriptYotubeApi')[0];
        if($scriptYotubeApi != undefined){
          createVideo();
        }else {
          var tag = document.createElement('script');
          tag.classList.add('scriptYotubeApi');
          tag.src = "https://www.youtube.com/iframe_api";
          var firstScriptTag = document.getElementsByTagName('script')[0];
          firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

          $window.onYouTubeIframeAPIReady = function() {
            createVideo();
          };
        }
      }
      function onPlayerReady(event) {
        window.eotm.thumbsContainerAnimate( true ,'videoAPI');
      }
    }
  }
});
