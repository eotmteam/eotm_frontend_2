var breath = angular.module('eotmApp.articles.breath', [
  'ui.router',
  'ngAnimate'
]);

//**********CONTROLLERS*******************************************

breath.controller('breathPageCtrl', ['$scope', '$document' ,'$stateParams','$filter' , '$timeout', breathPageCtrl]);


//*************************************
//breathPageCtrl CTRL
//*************************************

function breathPageCtrl($scope, $document ,$stateParams, $filter, $timeout){
  var vm = this;

  vm.interval = 10000;

};//BreathCtrl

eotmApp.directive('audioPlay', function(){
  return{
    link: function(scope, element, attrs){

     function play(){
       if(scope.vm.interval < 1001) scope.vm.interval = 2000;
        setTimeout(function(){
          element[0].pause();
          element[0].currentTime = 0;
          element[0].play();
          play();
        }, scope.vm.interval);

      }

      play();

      // chameleon nice gradients
      // https://codepen.io/teddyjefferson/pen/eNqKEP

    }
  }
});
