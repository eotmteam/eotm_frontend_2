var thumbs = angular.module('eotmApp.thumbs', [

]);

thumbs.service('thumbsData',['$http' , thumbsData]);
thumbs.controller('thumbsCtrl', ['$scope','thumbsData','$stateParams','$state', '$rootScope', '$timeout' , thumbsCtrl]);


function thumbsData($http) {
  var model = this;
  var URLS = {
    FETCH: 'data/articles.json'
  };
  var thumbs;
  model.getThumbsData = function() {
    return $http.get(URLS.FETCH);
  };
};

function thumbsCtrl($scope, thumbsData, $stateParams, $state, $rootScope, $timeout ){
  var vm = this;
  thumbsData.getThumbsData().then(function(result){
    vm.thumbs = result.data;
    vm.thumbsAll = result.data;
  });

  vm.loadMoreLimit = 12; // set to 6 in mobile? no need
  vm.tFilterC;
  vm.tFilterS;
  vm.thumbsAppear = false;
  vm.search = null;

  $scope.$watch($scope.$parent.vm.firstLoadOn,function() {
    vm.thumbsAppear = true;
  })

  $scope.$on('menuChange', function(event, data){
    vm.tFilterC = data.menuMain;
    vm.tFilterS = data.menuSub;
  })

  $scope.$on('searchMenu', function(event, data){
    vm.search = data;
  })

  vm.articleLoad = function (articleHref, isUnique) {
    var timeOutArticleLoad;
    if(window.scrollY != 0){
      timeOutArticleLoad = 350;
    }else{
      timeOutArticleLoad = 0;
    }
    setTimeout(function(){
      if(isUnique == true){
        $state.go('unique', {unique : articleHref});
      }else{
        $state.go('article', {article : articleHref});
      }
    },timeOutArticleLoad - 150);
  };

};

thumbs.filter('tagsFilter', function() {

  return function(input, searchTags) {
    var thumbsAll = input;
    var thumbsFiltered = [];
    if(thumbsAll && searchTags && searchTags.length > 0){

      thumbsAll.forEach(function(i){
        i.tags.forEach(function(tag){
          if(searchTags.includes(tag)){
            if(!thumbsFiltered.includes(i)){
              thumbsFiltered.push(i);
            }
          }
        });
      });
    }else{
      thumbsFiltered = thumbsAll;
    }
    return thumbsFiltered;
  }
});

thumbs.filter('wordFilter', function() {

  return function(input, searchWord) {
    var searchFields = [ 'headline', 'thumbDesc', 'description', 'artist','vidDesc', 'vidLink', 'source' ];
    if(!searchWord) return input;

    var articlesAll = input;
    var articlesFiltered = [];
      articlesAll.forEach(function(a){
        searchFields.forEach(function(f){
          if(!a[f]) return;
          var fieldContent = a[f].toLowerCase();
          if(fieldContent.includes(searchWord.toLowerCase())){
            if(!articlesFiltered.includes(a)) articlesFiltered.push(a);
          }
        })
      });

    return articlesFiltered;
  }
});


thumbs.directive("loadMoreThumbs", function ($window, $document) { // scroll listener for LOADMOREE
return{
  link: function(scope, element, attrs) {
    var bodyHeight = document.body.scrollHeight;
    var moreIndex = 12;
    var loadMore = function() {
      scope.vm.loadMoreLimit += moreIndex;
    };
    angular.element($window).bind("scroll", function() {
      bodyHeight = document.body.scrollHeight;
      if ($window.pageYOffset + $window.innerHeight + 50 > bodyHeight){
        loadMore();
        scope.$apply();
      }
    });
  }
}
});
